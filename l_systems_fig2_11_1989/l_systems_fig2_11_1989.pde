/* Programa para escribir cadenas para interpretación en
sistemas L con el sistema de tortuga, 
Referencia: Prusinkiewicz, Przemyslaw, y Aristid Lindenmayer. 1996. 
The algorithmic beauty of plants. Berlin, DE: Springer. P.7

Autores
Pablo Sanchez Mejía 
Valentina Vasco Restrepo  
Ricardo Cedeño Montaña 
Semillero Imagen Algoritmo 
Facultad de Comunicaciones y Filología 
Universidad de Antioquia 2024
*/

//Importar librería para exportar archivos svg
import processing.svg.*; 

//Definiciones
/*
Estas definiciones corresponden en el libro de referencia a: 
fig 2.11: A comparison of fractals F.M. Dekking 1982 Ter Dragon
Advance in Mathematics 44, 78-104 (1982)

*/
//Axioma
String w = "F+F+F";
//Variables para copiar el axioma y crear la cadena final
String wCopy = w;
String wFinal = wCopy;
//Reglas
String f = "f";
String F = "F-F+F";

//Iteraciones
int n = 5;
//Longitud de la línea
int lenF = 20;
//Ángulo
float d = radians(120.0);

void setup() {
  //Configuración básica
  size(1000, 1000);
  background(255);
  //Iniciar a exportar archivo SVG
  beginRecord(SVG, "fig_2.11_Dekking.svg");
  //Llamar la función para escribir cadenas.
  stringWriter(n, wCopy);
  println("Cadena: "+ wFinal);
  //Llamar la función para graficar.
  drawer();
  //Terminar de exportar archivo SVG.
  endRecord();
}  
