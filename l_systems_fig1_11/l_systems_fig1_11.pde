/* Programa para escribir cadenas para interpretación en
sistemas L con el sistema de tortuga, 
Referencia: Prusinkiewicz, Przemyslaw, y Aristid Lindenmayer. 1996. 
The algorithmic beauty of plants. Berlin, DE: Springer. P.7

Autores
Pablo Sanchez Mejía 
Valentina Vasco Restrepo  
Ricardo Cedeño Montaña 
Semillero Imagen Algoritmo 
Facultad de Comunicaciones y Filología 
Universidad de Antioquia 2024
*/

//Importar librería para exportar archivos svg
import processing.svg.*; 

//Definiciones
/*
Estas definiciones corresponden en el libro de referencia a: 
fig 1.11: Curves FASS (p. 12)

Este código permite realizar la fig 1.11
Axiomas
(a) String w = "L";
(b) String w = "-R";

reglas
(a) String L = "L+R++R-L--LL-R+";
(a) String R = "-L+RR++R+L--L-R";
(b) String L = "LL-R-R+L+L-R-RL+R+LLR-L+R+LL+R-LR-R-L+L+RR-";
(b) String R = "+LL-R-R+L+LR+L-RR-L-R+LRR-L-RL+L+R-R-L+L+RR";
*/
//Axioma
String w = "-R";
//Variables para copiar el axioma y crear la cadena final
String wCopy = w;
String wTemp = "";
String wFinal = "";
//Reglas
String F;
String f = "";
String L = "LL-R-R+L+L-R-RL+R+LLR-L+R+LL+R-LR-R-L+L+RR-";
String R = "+LL-R-R+L+LR+L-RR-L-R+LRR-L-RL+L+R-R-L+L+RR";
//Iteraciones
int n = 2;
//Longitud de la línea
int lenF = 12;
//Ángulo
float d = radians(90.0);

void setup() {
  //Configuración básica
  size(1000, 1000);
  background(255);
  //Iniciar a exportar archivo SVG
  beginRecord(SVG, "fig_1.11b.svg");
  //Llamar la función para escribir cadenas.
  stringWriter(n, wCopy);
  println("Cadena: "+ wFinal);
  //Llamar la función para graficar.
  drawer();
  //Terminar de exportar archivo SVG.
  endRecord();
}  
