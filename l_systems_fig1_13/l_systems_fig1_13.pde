/* Programa para escribir cadenas para interpretación en
sistemas L con el sistema de tortuga, 
Referencia: Prusinkiewicz, Przemyslaw, y Aristid Lindenmayer. 1996. 
The algorithmic beauty of plants. Berlin, DE: Springer. P.7

Autores
Pablo Sanchez Mejía 
Valentina Vasco Restrepo  
Ricardo Cedeño Montaña 
Semillero Imagen Algoritmo 
Facultad de Comunicaciones y Filología 
Universidad de Antioquia 2024
*/

//Importar librería para exportar archivos svg
import processing.svg.*; 

//Definiciones
/*
Estas definiciones corresponden en el libro de referencia a: 
fig 1.13 a y b: Curves FASS (p. 14)

Este código permite realizar las fig 1.13a y 1.13b
Axiomas
(a) String w = "L";
(b) String w = "-R";

reglas
(a) String L = "+RRR-LL-L-R-L+RR+LR-L+R+LRL+LLL-RR-L-L-L-RL+R+LLL-LL-R+R-L-LR+R-LL-L-R+L-LL+LLL";
(a) String R = "RRR-LL-L-R-L+RR+LR-L+R+LRL+LLL-RR-L-L-L-RL+R+LLL-LL-R+R-L-LR+R-LL-L-R+L-LL+LLL+";
(b) String L = "+RR-L-L+R+RL+R-LR+L-R-L+RLL-R-LRLRL+R-LRLR+L+RRLRL+R-LRL-L-RL+R-LRLRL+R+RLRLRL+R-LL-R-L+RLL-R-LR+R+L-L-R+R+LL-R-LR+R+L-L-R+R+LL";
(b) String R = "RR-L-L+R+R-L-LR+L+RR-L-L+R+R-L-LR+L+RRL-R+L+RR+L-RLRLRL-L-RLRLR+L-RL+R+RLR+L-RLRLL-R-LRLR+L-RLRLR+L+RRL-R+L+R-LR+L-RL-L-R+R+LL-";
*/
//Axioma
String w = "R";
//Variables para copiar el axioma y crear la cadena final
String wCopy = w;
String wTemp = "";
String wFinal = "";
//Reglas
String F;
String f = "";
String L = "+RR-L-L+R+RL+R-LR+L-R-L+RLL-R-LRLRL+R-LRLR+L+RRLRL+R-LRL-L-RL+R-LRLRL+R+RLRLRL+R-LL-R-L+RLL-R-LR+R+L-L-R+R+LL-R-LR+R+L-L-R+R+LL";
String R = "RR-L-L+R+R-L-LR+L+RR-L-L+R+R-L-LR+L+RRL-R+L+RR+L-RLRLRL-L-RLRLR+L-RL+R+RLR+L-RLRLL-R-LRLR+L-RLRLR+L+RRL-R+L+R-LR+L-RL-L-R+R+LL-";

//Iteraciones
int n = 2;
//Longitud de la línea
int lenF = 10;
//Ángulo
float d = radians(90.0);

void setup() {
  //Configuración básica
  size(1000, 1000);
  background(255);
  //Iniciar a exportar archivo SVG
  beginRecord(SVG, "fig_1.13b.svg");
  //Llamar la función para escribir cadenas.
  stringWriter(n, wCopy);
  println("Cadena: "+ wFinal);
  //Llamar la función para graficar.
  drawer();
  //Terminar de exportar archivo SVG.
  endRecord();
}  
