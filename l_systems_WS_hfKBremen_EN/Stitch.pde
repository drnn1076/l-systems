/* 
License
   Program to write strings for interpretation on L-systems with 
   output to a SVG vector graphics file
   Copyright (C) 2024  Ricardo Cedeño Montaña, Pablo Sanchez Mejia and Valentina Vasco Restrepo

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

//Function to draw the stitches
void puntada(int pun, boolean [] l){
  boolean[] lados = l;
  int anchoP = int(width*.04); 
  int len = width-(anchoP*2);
  int p = pun;
  //Crear objetos puntada
  Frame[] puntada = new Frame[(len/anchoP-2)];
  
  if(lados[0]==true){
    //Horizontal frame line up 
    for (int i=0;i<(len/anchoP)-2; i++){
        puntada[i] = new Frame(anchoP);
        pushMatrix();
          translate((i*anchoP)+(anchoP*2), anchoP);

          switch(p) {
            case 1: 
              puntada[i].drawFestonUp();
              break;
            case 2: 
              puntada[i].drawFestonDown();
              break;
            case 3: 
              puntada[i].drawMoscaUp();
              break;
            case 4: 
              puntada[i].drawMoscaDown();
              break;
            case 5: 
              puntada[i].drawRamas();
              break;
            default:
              puntada[i].drawCruz();
              break;
          }
          
        popMatrix();  
    }
  }
  if(lados[1]==true){
    //Vertical frame line right  
    for (int i=0;i<(len/anchoP)-2; i++){
        puntada[i] = new Frame(anchoP);
        pushMatrix();
          translate(width-(anchoP), (i*anchoP)+(anchoP*2));
          rotate(radians(90));
          switch(p) {
            case 1: 
              puntada[i].drawFestonUp();
              break;
            case 2: 
              puntada[i].drawFestonDown();
              break;
            case 3: 
              puntada[i].drawMoscaUp();
              break;
            case 4: 
              puntada[i].drawMoscaDown();
              break;
            case 5: 
              puntada[i].drawRamas();
              break;
            default:
              puntada[i].drawCruz();
              break;
          }
        popMatrix();  
    }
   }
   if(lados[2]){
    //Horizontal frame line bottom 
    for (int i=0;i<len/anchoP-2; i++){
        puntada[i] = new Frame(anchoP);
        pushMatrix();
          translate((i*anchoP)+(anchoP*3), height-(anchoP));
          rotate(radians(180));
          switch(p) {
            case 1: 
              puntada[i].drawFestonUp();
              break;
            case 2: 
              puntada[i].drawFestonDown();
              break;
            case 3: 
              puntada[i].drawMoscaUp();
              break;
            case 4: 
              puntada[i].drawMoscaDown();
              break;
            case 5: 
              puntada[i].drawRamas();
              break;
            default:
              puntada[i].drawCruz();
              break;
          }
        popMatrix();  
    }
   }
   if(lados[3]==true){
    //Vertical frame line left 
    for (int i=0;i<(len/anchoP)-2; i++){
        puntada[i] = new Frame(anchoP);
        pushMatrix();
          translate(anchoP, (i*anchoP)+(anchoP*3));
          rotate(radians(270));
          switch(p) {
            case 1: 
              puntada[i].drawFestonUp();
              break;
            case 2: 
              puntada[i].drawFestonDown();
              break;
            case 3: 
              puntada[i].drawMoscaUp();
              break;
            case 4: 
              puntada[i].drawMoscaDown();
              break;
            case 5: 
              puntada[i].drawRamas();
              break;
            default:
              puntada[i].drawCruz();
              break;
          }
        popMatrix();  
      }
    }
  }
