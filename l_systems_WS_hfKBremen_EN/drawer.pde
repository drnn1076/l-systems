/* 
License
   Program to write strings for interpretation on L-systems with 
   output to a SVG vector graphics file
   Copyright (C) 2024  Ricardo Cedeño Montaña, Pablo Sanchez Mejia and Valentina Vasco Restrepo

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

//Function to draw the tree 
void drawer (int lenF){

  translate(width/2, height/1.15);
  rotate(radians(-90));
  pushMatrix();

  for (int i = 0; i < wFinal.length(); i++) {
    char c = wFinal.charAt(i);
    //int lenF = int(random(9,20));
    float d = radians(random(22.5, 45.5));
    
    if (c == 'F') {
      stroke(#000000);
      strokeWeight(1);
      line(0, 0, lenF, 0);
      strokeWeight(3);
      point(lenF,0);
      translate(lenF, 0);
      
    } else if (c == 'f') {
      noStroke();
      line(0, 0, lenF, 0);
      translate(lenF, 0);
      
    } else if (c == 'L') {
      stroke(#000000);
      //line(0, 0, lenF, 0);
      //translate(lenF, 0);
    
    } else if (c == 'R') {
      stroke(#000000);
      //line(0, 0, lenF, 0);
      //translate(lenF, 0);
    
    } else if (c == '+') {
      //float d = radians(random(25, 45));
      rotate (d);
      
    } else if (c == '-') {
      //float d = radians(random(25, 45));
      rotate (-d);
      
    } else if (c == '[') {
         pushMatrix();
         
    } else if (c == ']') {
        popMatrix();
    }
  }
  popMatrix();
}
