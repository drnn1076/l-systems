/* 
License
   Program to write strings for interpretation on L-systems with 
   output to a SVG vector graphics file
   Copyright (C) 2024  Ricardo Cedeño Montaña, Pablo Sanchez Mejia and Valentina Vasco Restrepo

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.

Title
Program to write strings for interpretation on
L-systems with the turtle system, 
Reference: Prusinkiewicz, Przemyslaw, and Aristid Lindenmayer. 1996. 
The algorithmic beauty of plants. Berlin, DE: Springerz.
*/

/* Authors
Ricardo Cedeño Montaña 
Pablo Sanchez Mejía 
Valentina Vasco Restrepo  
Media Archaeology Laboratory
Faculty of Communications and Philology 
University of Antioquia 2024
*/

/* Definitions
The definitions used in this sketch correspond in the reference book to: fig 1.24. 
reference book to: fig 1.24: bracketed OL-Systems
Each case requires  
- Axiom
- Rulers
- Angle
- Iterations
- Length of the section
*/

/*Notes:
In the drawer the sections of R and L must have no line or translation for the curve to be proportional.
translation for the curve to be proportional.
*/

//Import library for exporting svg files

import processing.svg.*; 

String wTemp = "";
String wFinal = "";

void setup() {
  //Basic setup
  size(500, 500);
  background(255);
  String w,F,f,L,R;
  float d = 0;
  int n = 0; 
  int lenF = 0;
  
 /*Instructions for use
   A. For the tree 
   1. Choose a motif from 0 to 5 in 'int tree'.
  
   B. For the frame
   1.  Choose the sides of the frame that you want to embroider by 
       by setting true in 'boolean[] sides'. 
       Do this in a clockwise direction 
       starting with the top side.
   2.  Choose a stitch from 0 to 5 in 'int stitch'; 
       use the reference in 'String[] stitches' like this: 
       cross = 0, scallopUp = 1,..., branches =5.
  */

  int arbol = 3;
  boolean[] lados = {true, true, true, true};
  String[] puntadas = {"cruz", "festonUp", "festonDown",
          "moscaUp", "moscaDown", "ramas"};
  int puntada = 3;
  
  switch(arbol) {
            case 1: 
                //Axiom
                w = "F";
                //Rules
                F = "F[+F]F[-F][F]";
                f = "";
                L = "";
                R = "";
                //Angle
                d = radians(20);
                //Iterations
                n = 3;
                //Length of the line
                lenF = 24;
                break;
            case 2:
                //Axiom
                 w = "F";
                //Rules
                 F = "FF-[-F+F]+[+F-F]";
                 f = "";
                 L = "";
                 R = "";
                 //Angle
                 d = radians(20);
                 //Iterations
                 n = 3;
                 //Length of the line
                 lenF = 18;
                 break;
            case 3: 
                //Axiom
                 w = "L";
                //Rules
                 F = "FF";
                 f = "";
                 L = "F[+L]F[-L]+L";
                 R = "";
                 //Angle
                 d = radians(20);
                 //Iterations
                 n = 4;
                 //Length of the line
                 lenF = 13;
                 break;
            case 4: 
                 //Axiom
                 w = "L";
                 //Rules
                 F = "FF";
                 f = "";
                 L = "F[+L][-L]FL";
                 R = "";
                 //Angle
                 d = radians(26);
                 //Iterations
                 n = 4;
                 //Length of the line
                 lenF = 12;
                 break;
            case 5: 
                 //Axiom
                 w = "L";
                 //Rules
                 F = "FF";
                 f = "";
                 L = "F-[[L]+L]+F[+FL]-L";
                 R = "";
                 //Angle
                 d = radians(18);
                 //Iterations
                 n = 4;
                 //Length of the line
                 lenF = 10;
                 break;
            default:
                 //Axiom
                 w = "F";
                 //Rules
                 F = "F[+F]F[-F]";
                 f = "";
                 L = "";
                 R = "";
                 //Angle
                 d = radians(30);
                 //Iterations
                 n = 4;
                 //Length of the line
                 lenF = 15;
                 break;
          }
  //Variables to copy the axiom and generate the final string
  String wCopy = w;
  //Record the SVG file
  beginRecord(SVG, "fig_HfK_Bremen_202501.svg");
  
  //Add crop lines 
  int margin =5;
  line(margin,margin,margin*2,margin);
  line(margin, margin, margin, margin*2);
  line(width-margin,margin,width-(margin*2),margin);
  line(width-margin,margin,width-margin,margin*2);
  line(margin,height-margin,margin*2,height-margin);
  line(margin, height-margin, margin, height-(margin*2));
  line(width-margin,height-margin,width-(margin*2),height-margin);
  line(width-margin,height-margin,width-margin,height-(margin*2));
  
  //Call the frame function
  puntada(puntada, lados);
  
  //Call the string writer function
  stringWriter(n, wCopy, w, F, f, L, R);
  println("Cadena: "+ wFinal);
  //Call the drawer function 
  drawer(lenF);
  
  //Finish the SVG file
  endRecord();
} 
