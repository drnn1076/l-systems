/* 
License
   Program to write strings for interpretation on L-systems with 
   output to a SVG vector graphics file
   Copyright (C) 2024  Ricardo Cedeño Montaña, Pablo Sanchez Mejia and Valentina Vasco Restrepo

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

//Class to draw the frame
//It contains six stitches

/* Note:
The universal stitch is still in development
*/
class Frame{
  int anchoP;
  int x, y;
  
  Frame(int a){
    anchoP = a;
  }
  
  //Cross stitch
  void drawCruz(){
    line(0, 0, anchoP, anchoP);
    line(anchoP, 0, 0, anchoP);
  }
  //Blanket down stitch
  void drawFestonDown(){
    line(0, 0, anchoP, 0);
    line(0, 0, 0, anchoP);
  }
  //Blanket up stitch
  void drawFestonUp(){
    line(0, 0, 0, anchoP);
    line(0, anchoP, anchoP, anchoP);
  }
  //Fly down stitch
  void drawMoscaDown(){
    line(0, 0, anchoP/2, anchoP/2);
    line(anchoP/2, anchoP/2, anchoP, 0);
    line(anchoP/2, anchoP/2, anchoP/2, anchoP);
  }
  //Fly up stitch
  void drawMoscaUp(){
    line(0, anchoP, anchoP/2, anchoP/2);
    line(anchoP/2, anchoP/2, anchoP, anchoP);
    line(anchoP/2, anchoP/2, anchoP/2, 0);
  }
  //Branches stitch
  void drawRamas(){
    line(anchoP/2, anchoP, 0, anchoP/2);
    line(0, anchoP/2, anchoP, anchoP/2);
    line(anchoP/2, anchoP/2, anchoP, 0);
  }
  //Universal stitch 
  void drawUniversal(){
    line(0, anchoP, anchoP, 0);
    line(anchoP, 0, anchoP*2, anchoP);
    line(anchoP/2, 0, anchoP*1.5, 0);
    line(0, anchoP, anchoP/2, anchoP);
    line(anchoP*1.5, anchoP, anchoP*2, anchoP);
  }
  
}

  
