/* 
License
   Program to write strings for interpretation on L-systems with 
   output to a SVG vector graphics file
   Copyright (C) 2024  Ricardo Cedeño Montaña, Pablo Sanchez Mejia and Valentina Vasco Restrepo

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

//Funtion to generate the strings

String stringWriter(int n, String wCopy, String w, String F, String f, String L, String R){
  //para iteraciones superiores a 0
  if (n > 0){
    //Decreases the iteration by 1
    n = n - 1;
    //Replaces the axiom with the rules
    // and writes them in a temp working variable
    for (int j = 0; j < wCopy.length(); j++) {
      char c = wCopy.charAt(j);
      if (c == 'F') {
        wTemp += F;
      } else if (c == 'f') {
        wTemp += f;
      } else if (c == 'L') {
        wTemp += L;
      } else if (c == 'R') {
        wTemp += R;
      } else {
        wTemp += c;      
      }
    }
    //Copies the temp string to the working variable
    wCopy = wTemp;
   
   //If it is the last iteration, writes the final string  
    if(n==0){
      wFinal = wCopy;
    }
    //Empties the temp working variable
    wTemp = "";
    
    //Calls the recursive function 
    //until it is less than 0 
    stringWriter(n, wCopy, w, F, f, L, R); 
  }
  //Returns the final string
  return wFinal;
}
