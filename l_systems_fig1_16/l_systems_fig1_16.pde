/* Programa para escribir cadenas para interpretación en
sistemas L con el sistema de tortuga, 
Referencia: Prusinkiewicz, Przemyslaw, y Aristid Lindenmayer. 1996. 
The algorithmic beauty of plants. Berlin, DE: Springer. P.7

Autores
Pablo Sanchez Mejía 
Valentina Vasco Restrepo  
Ricardo Cedeño Montaña 
Semillero Imagen Algoritmo 
Facultad de Comunicaciones y Filología 
Universidad de Antioquia 2024
*/

//Importar librería para exportar archivos svg
import processing.svg.*; 

//Definiciones
/*
Estas definiciones corresponden en el libro de referencia a: 
fig 1.16a: FASS curves 3x3 tiles form a mocrotile (p. 17)
fig 1.16b: FASS curves 4x4 tiles form a macrotile (p. 17)

1.16 a 
Axioma
String w = "R";
Reglas
String L = "LF+RFR+FL-F-LFLFL-FRFR+";
String R = "-LFLF+RFRFR+F+RF-LFL-FR";

1.16 b 
Axioma 
String = "-L";
Reglas
String L = "LFLF+RFR+FLFL-FRF-LFL-FR+F+RF-LFL-FRFRFR+";
String R = "-LFLFLF+RFR+FL-F-LF+RFR+FLF+RFRF-LFL-FRFR";

*/
//Axioma
String w = "R";
//Variables para copiar el axioma y crear la cadena final
String wCopy = w;
String wTemp = "";
String wFinal = "";
//Reglas
String F = "F";
String f = "";
String L = "LF+RFR+FL-F-LFLFL-FRFR+";
String R = "-LFLF+RFRFR+F+RF-LFL-FR";

/*Notas:
En el drawer los tramos de R y L no deben tener linea ni
translación para que la curva sea proporcial.
*/

//Iteraciones
int n = 3;
//Longitud de la línea
int lenF = 8;
//Ángulo
float d = radians(90.0);

void setup() {
  //Configuración básica
  size(1000, 1000);
  background(255);
  //Iniciar a exportar archivo SVG
  //beginRecord(SVG, "fig_1.16.svg");
  //Llamar la función para escribir cadenas.
  stringWriter(n, wCopy);
  println("Cadena: "+ wFinal);
  //Llamar la función para graficar.
  drawer();
  //Terminar de exportar archivo SVG.
  endRecord();
}  
