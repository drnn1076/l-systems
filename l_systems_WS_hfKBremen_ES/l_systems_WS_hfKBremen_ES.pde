/* Programa para escribir cadenas para interpretación en
sistemas L con el sistema de tortuga, 
Referencia: Prusinkiewicz, Przemyslaw, y Aristid Lindenmayer. 1996. 
The algorithmic beauty of plants. Berlin, DE: Springer. P.7
*/

/* Autores
Ricardo Cedeño Montaña 
Pablo Sanchez Mejía 
Valentina Vasco Restrepo  
Laboratorio de arqueología de medios enmedio
Facultad de Comunicaciones y Filología 
Universidad de Antioquia 2024
*/

/* Definiciones
Las definiciones usadas en este sketch corresponden en el 
libro de referencia a: fig 1.24: bracketed OL-Systems
Cada caso requiere de  
- Axioma
- Reglas
- Ángulo
- Iteraciones
- Longitud del tramo
*/

/*Notas:
En el drawer los tramos de R y L no deben tener linea ni
translación para que la curva sea proporcial.
*/

//Importar librería para exportar archivos svg
import processing.svg.*; 

String wTemp = "";
String wFinal = "";

void setup() {
  //Configuración básica
  size(500, 500);
  background(255);
  String w,F,f,L,R;
  float d = 0;
  int n = 0; 
  int lenF = 0;
  
 /*Instrucciones de uso
   A. Para el árbol 
   1. Escoja un motivo de 0 a 5 en "int arbol".
  
   B. Para el marco
   1.  Escoja los lados del marco que quiera bordar 
       marcando true en 'boolean[] lados'. 
       Hacerlo en sentido de las manecillas del reloj 
       empezando por el lado de arriba.
   2.  Escoja una puntada de 0 a 5 en "int puntada"; 
       usar la referencia en "String[] puntadas" así: 
       cruz = 0, festonUp = 1,..., ramas =5.
  */
  int arbol = 5;
  boolean[] lados = {true, true, true, true};
  String[] puntadas = {"cruz", "festonUp", "festonDown",
          "moscaUp", "moscaDown", "ramas"};
  int puntada = 5;
  
  switch(arbol) {
            case 1: 
                //Axioma
                w = "F";
                //Reglas
                F = "F[+F]F[-F][F]";
                f = "";
                L = "";
                R = "";
                //Ángulo
                d = radians(20);
                //Iteraciones
                n = 3;
                //Longitud de la línea
                lenF = 24;
                break;
            case 2:
                //Axioma
                 w = "F";
                //Reglas
                 F = "FF-[-F+F]+[+F-F]";
                 f = "";
                 L = "";
                 R = "";
                 //Ángulo
                 d = radians(20);
                 //Iteraciones
                 n = 3;
                 //Longitud de la línea
                 lenF = 18;
                 break;
            case 3: 
                //Axioma
                 w = "L";
                //Reglas
                 F = "FF";
                 f = "";
                 L = "F[+L]F[-L]+L";
                 R = "";
                 //Ángulo
                 d = radians(20);
                 //Iteraciones
                 n = 4;
                 //Longitud de la línea
                 lenF = 13;
                 break;
            case 4: 
                 //Axioma
                 w = "L";
                 //Reglas
                 F = "FF";
                 f = "";
                 L = "F[+L][-L]FL";
                 R = "";
                 //Ángulo
                 d = radians(26);
                 //Iteraciones
                 n = 4;
                 //Longitud de la línea
                 lenF = 12;
                 break;
            case 5: 
                 //Axioma
                 w = "L";
                 //Reglas
                 F = "FF";
                 f = "";
                 L = "F-[[L]+L]+F[+FL]-L";
                 R = "";
                 //Ángulo
                 d = radians(18);
                 //Iteraciones
                 n = 4;
                 //Longitud de la línea
                 lenF = 10;
                 break;
            default:
                 //Axioma
                 w = "F";
                 //Reglas
                 F = "F[+F]F[-F]";
                 f = "";
                 L = "";
                 R = "";
                 //Ángulo
                 d = radians(30);
                 //Iteraciones
                 n = 4;
                 //Longitud de la línea
                 lenF = 15;
                 break;
          }
  //Variables para copiar el axioma y crear la cadena final
  String wCopy = w;
  //Iniciar a exportar archivo SVG
  beginRecord(SVG, "fig_HfK_Bremen_202501.svg");
  
  //Añadir lineas de corte
  int margin =5;
  line(margin,margin,margin*2,margin);
  line(margin, margin, margin, margin*2);
  line(width-margin,margin,width-(margin*2),margin);
  line(width-margin,margin,width-margin,margin*2);
  line(margin,height-margin,margin*2,height-margin);
  line(margin, height-margin, margin, height-(margin*2));
  line(width-margin,height-margin,width-(margin*2),height-margin);
  line(width-margin,height-margin,width-margin,height-(margin*2));
  
  //Llamar la función del marco
  puntada(puntada, lados);
  
  //Llamar la función para escribir cadenas.
  stringWriter(n, wCopy, w, F, f, L, R);
  println("Cadena: "+ wFinal);
  //Llamar la función para graficar.
  drawer(lenF);
  
  //Terminar de exportar archivo SVG.
  endRecord();
} 
