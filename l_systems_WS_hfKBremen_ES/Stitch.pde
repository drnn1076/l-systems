//Función para dibujar las puntadas del marco
void puntada(int pun, boolean [] l){
  boolean[] lados = l;
  int anchoP = int(width*.04); 
  int len = width-(anchoP*2);
  int p = pun;
  //Crear objetos puntada
  Frame[] puntada = new Frame[(len/anchoP-2)];
  
  if(lados[0]==true){
    //Linea de marco horizonal arriba
    for (int i=0;i<(len/anchoP)-2; i++){
        puntada[i] = new Frame(anchoP);
        pushMatrix();
          translate((i*anchoP)+(anchoP*2), anchoP);

          switch(p) {
            case 1: 
              puntada[i].drawFestonUp();
              break;
            case 2: 
              puntada[i].drawFestonDown();
              break;
            case 3: 
              puntada[i].drawMoscaUp();
              break;
            case 4: 
              puntada[i].drawMoscaDown();
              break;
            case 5: 
              puntada[i].drawRamas();
              break;
            default:
              puntada[i].drawCruz();
              break;
          }
          
        popMatrix();  
    }
  }
  if(lados[1]==true){
    //Linea vertical derecha del marco 
    for (int i=0;i<(len/anchoP)-2; i++){
        puntada[i] = new Frame(anchoP);
        pushMatrix();
          translate(width-(anchoP), (i*anchoP)+(anchoP*2));
          rotate(radians(90));
          switch(p) {
            case 1: 
              puntada[i].drawFestonUp();
              break;
            case 2: 
              puntada[i].drawFestonDown();
              break;
            case 3: 
              puntada[i].drawMoscaUp();
              break;
            case 4: 
              puntada[i].drawMoscaDown();
              break;
            case 5: 
              puntada[i].drawRamas();
              break;
            default:
              puntada[i].drawCruz();
              break;
          }
        popMatrix();  
    }
   }
   if(lados[2]){
    //Linea horizonal abajo del marco 
    for (int i=0;i<len/anchoP-2; i++){
        puntada[i] = new Frame(anchoP);
        pushMatrix();
          translate((i*anchoP)+(anchoP*3), height-(anchoP));
          rotate(radians(180));
          switch(p) {
            case 1: 
              puntada[i].drawFestonUp();
              break;
            case 2: 
              puntada[i].drawFestonDown();
              break;
            case 3: 
              puntada[i].drawMoscaUp();
              break;
            case 4: 
              puntada[i].drawMoscaDown();
              break;
            case 5: 
              puntada[i].drawRamas();
              break;
            default:
              puntada[i].drawCruz();
              break;
          }
        popMatrix();  
    }
   }
   if(lados[3]==true){
    //Linea vertical izquierda del marco 
    for (int i=0;i<(len/anchoP)-2; i++){
        puntada[i] = new Frame(anchoP);
        pushMatrix();
          translate(anchoP, (i*anchoP)+(anchoP*3));
          rotate(radians(270));
          switch(p) {
            case 1: 
              puntada[i].drawFestonUp();
              break;
            case 2: 
              puntada[i].drawFestonDown();
              break;
            case 3: 
              puntada[i].drawMoscaUp();
              break;
            case 4: 
              puntada[i].drawMoscaDown();
              break;
            case 5: 
              puntada[i].drawRamas();
              break;
            default:
              puntada[i].drawCruz();
              break;
          }
        popMatrix();  
      }
    }
  }
