//función graficadora del árbol
void drawer (int lenF){

  translate(width/2, height/1.15);
  rotate(radians(-90));
  pushMatrix();

  for (int i = 0; i < wFinal.length(); i++) {
    char c = wFinal.charAt(i);
    //int lenF = int(random(9,20));
    float d = radians(random(22.5, 45.5));
    
    if (c == 'F') {
      stroke(#000000);
      strokeWeight(1);
      line(0, 0, lenF, 0);
      strokeWeight(3);
      point(lenF,0);
      translate(lenF, 0);
      
    } else if (c == 'f') {
      noStroke();
      line(0, 0, lenF, 0);
      translate(lenF, 0);
      
    } else if (c == 'L') {
      stroke(#000000);
      //line(0, 0, lenF, 0);
      //translate(lenF, 0);
    
    } else if (c == 'R') {
      stroke(#000000);
      //line(0, 0, lenF, 0);
      //translate(lenF, 0);
    
    } else if (c == '+') {
      //float d = radians(random(25, 45));
      //println(d);
      rotate (d);
      
    } else if (c == '-') {
      //float d = radians(random(25, 45));
      rotate (-d);
      
    } else if (c == '[') {
         pushMatrix();
         
    } else if (c == ']') {
        popMatrix();
    }
  }
  popMatrix();
}
