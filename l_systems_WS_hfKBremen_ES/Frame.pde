//Clase para dibujar el marco del motivo
//Contiene seis tipos de puntadas

/* Nota:
La punta universal está aún en desarrollo, no usar.
*/
class Frame{
  int anchoP;
  int x, y;
  
  Frame(int a){
    anchoP = a;
  }
  
  //Puntada cruz
  void drawCruz(){
    line(0, 0, anchoP, anchoP);
    line(anchoP, 0, 0, anchoP);
  }
  //Puntada feston para abajo
  void drawFestonDown(){
    line(0, 0, anchoP, 0);
    line(0, 0, 0, anchoP);
  }
  //Puntada feston para arriba
  void drawFestonUp(){
    line(0, 0, 0, anchoP);
    line(0, anchoP, anchoP, anchoP);
  }
  //Puntada mosca para abajo
  void drawMoscaDown(){
    line(0, 0, anchoP/2, anchoP/2);
    line(anchoP/2, anchoP/2, anchoP, 0);
    line(anchoP/2, anchoP/2, anchoP/2, anchoP);
  }
  //Puntada mosca para arriba
  void drawMoscaUp(){
    line(0, anchoP, anchoP/2, anchoP/2);
    line(anchoP/2, anchoP/2, anchoP, anchoP);
    line(anchoP/2, anchoP/2, anchoP/2, 0);
  }
  //Puntada ramas
  void drawRamas(){
    line(anchoP/2, anchoP, 0, anchoP/2);
    line(0, anchoP/2, anchoP, anchoP/2);
    line(anchoP/2, anchoP/2, anchoP, 0);
  }
  //Puntada universal 
  void drawUniversal(){
    line(0, anchoP, anchoP, 0);
    line(anchoP, 0, anchoP*2, anchoP);
    line(anchoP/2, 0, anchoP*1.5, 0);
    line(0, anchoP, anchoP/2, anchoP);
    line(anchoP*1.5, anchoP, anchoP*2, anchoP);
  }
  
}

  
