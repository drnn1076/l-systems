String stringWriter(int n, String wCopy){
  //para iteraciones superiores a 0
  if (n > 0){
    //decrece la iteración en 1
    n = n - 1;
    //reemplaza los axiomas con las reglas 
    wCopy = wCopy.replace("F", F);
    wCopy = wCopy.replace("f", f);
    //copia el resultado en una cadena final
    wFinal = wCopy;
    //función recursiva hasta que la iteración
    //llegue a menor que 0
    stringWriter(n, wCopy); 
  }
  //retorna la cadena final
  return wFinal;
}
