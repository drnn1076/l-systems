/* Programa para escribir cadenas para interpretación en
sistemas L con el sistema de tortuga, 
Referencia: Prusinkiewicz, Przemyslaw, y Aristid Lindenmayer. 1996. 
The algorithmic beauty of plants. Berlin, DE: Springer. P.7

Autores
Pablo Sanchez Mejía 
Valentina Vasco Restrepo  
Ricardo Cedeño Montaña 
Semillero Imagen Algoritmo 
Facultad de Comunicaciones y Filología 
Universidad de Antioquia 2024
*/

//Importar librería para exportar archivos svg
import processing.svg.*; 

//Definiciones
/*
Estas definiciones corresponden en el libro de referencia a: 
fig 1.7: Examples of Koch curves generated using L-systems (p. 9)

Este código permite realizar las fig 1.7 (a) y (b)
Axiomas
(a) String w = "F-F-F-F"
(b) String w = "-F"
regla
(a) String F = "F+FF-FF-F-F+F+FF-F-F+F+FF+FF-F";
(b) String F = "F+F-F-F+F";
*/
//Axioma
String w = "-F";
//Variables para copiar el axioma y crear la cadena final
String wCopy = w;
String wFinal = wCopy;
//Reglas
String f = "";
String F = "F+F-F-F+F";

//Iteraciones
int n = 2;
//Longitud de la línea
int lenF = 12;
//Ángulo
float d = radians(90.0);

void setup() {
  //Configuración básica
  size(1000, 1000);
  background(255);
  stroke(#000000);
  //Iniciar a exportar archivo SVG
  beginRecord(SVG, "fig_1.7b.svg");
  //Llamar la función para escribir cadenas.
  stringWriter(n, wCopy);
  println("Cadena: "+ wFinal);
  //Llamar la función para graficar.
  drawer();
  //Terminar de exportar archivo SVG.
  endRecord();
}  
