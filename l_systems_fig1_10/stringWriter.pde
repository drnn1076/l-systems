String stringWriter(int n, String wCopy){
  //para iteraciones superiores a 0
  if (n > 0){
    //decrece la iteración en 1
    n = n - 1;
    //reemplaza el axioma de la variable de trabajo con las reglas
    // y las escribe en una variable temporal
    for (int j = 0; j < wCopy.length(); j++) {
      char c = wCopy.charAt(j);
      
      if (c == 'F') {
        wTemp += F;
      }
      else if (c == 'f') {
        wTemp += f;
      }
      else if (c == 'L') {
        wTemp += L;
      }
      else if (c == 'R') {
        wTemp += R;
      }
      else {
        wTemp += c;      
      }
    }
    //Copia la cadena temporal a la variable de trabajo
    wCopy = wTemp;
   
   //Si es la última iteración escribe la cadena final 
    if(n==0){
      wFinal = wCopy;
    }
    //Vacia la variable temporal
    wTemp = "";
    
    //función recursiva hasta que la iteración
    //llegue a menor que 0
    stringWriter(n, wCopy); 
  }
  //retorna la cadena final
  return wFinal;
}
