//Class to create an edge with the eight positions in a cell
class edge {
   
  int posOneX;
  int posOneY;
  int len;
  int posTwoX;
  int posTwoY;
  
  int nor;
  int norX;
  int norY;
  int side;
  String pos;
  
  edge (int x1, int y1, int n, String p) {     
    
    len = width/gridSize;
    posOneX = x1;
    posOneY = y1;
    posTwoX = x1+len;
    posTwoY = y1;
    nor = n;
 
    if(p=="R"){ side = 0;
       }else if(p=="D"){ side = 90;
       }else if(p=="L"){ side = 180;
       }else if(p=="U"){ side = 270;
       }else{println(p+" not valid");
     }
    
     if (nor == 0){
       norX = (posTwoX-posOneX)/2;
       norY = posOneY+textSize;
     }else if(nor == 1){
       norX = (posTwoX-posOneX)/2;
       norY = posOneY-textSize;
     }
     
  }
  
    int[] vEdge(){
      int[] edgeA = {posOneX, posOneY, nor, side};
      printArray(edgeA);
      return edgeA; 
    }
    
    void drawEdge(){

      int X1 = (posOneX*len)+len/2;
      int Y1 = (posOneY*len)+len/2;
      
      pushMatrix();
        translate(X1, Y1);
        strokeWeight(5);
        stroke(#007700, 60);
      
        pushMatrix();
          rotate(radians(side));
          line(0, 0, len, 0);
          if (nor==0){
              stroke(#0099FF);
            }else if(nor==1){
              stroke(#9900FF);
          }
          strokeWeight(8);
          point(norX, norY);
        popMatrix();
        
      popMatrix();
    }
    
  }
