//Class to create a graph's object based on the matrix
class Graph {
  IntList nodes;
  IntList cells;
  int counter = 1;
  int rows=0;
  
  Graph (IntList n, IntList c) {
    nodes = n;
    cells = c;
    for ( int i : nodes){
      counter = i%gridSize;
        if(counter==0){
      rows++;
    }
    
    /*Establish which nodes are in the corners, sides (but not corners), and
    middle of the matrix.
    */
  //Corner nodes
  if((rows==1 || rows==gridSize) && (counter==0 || counter==gridSize-1)){
      corners_l.add(i);
    }
  //Side nodes (except corners)
  if((rows==1) && (counter!=0 && counter!=gridSize-1)){   
      sides_l.add(i);
    }else if((rows!=1 && rows!=gridSize) && (counter==0 || counter==gridSize-1)){
      sides_l.add(i);
    }else if((rows==gridSize) && (counter!=0 && counter!=gridSize-1)){
      sides_l.add(i);
  }
  //Middle nodes
  if((rows!=1 && rows!=gridSize) && (counter!=0 && counter!=gridSize-1)){
      mids_l.add(i);
      }
    }
  }
  
  //Function to print lists for corners, sides, and middle nodes.
  void printLists(){
    println("corner nodes: "+corners_l);
    println("side nodes: "+sides_l);
    println("middle nodes: "+mids_l);
  }
  
  //Get list of corners
  ArrayList<Integer> getCorners(){
    return corners_l;
  }
  //Get list of sides
  ArrayList<Integer> getSides(){
    return sides_l;
  }
   //Get list of corners
  ArrayList<Integer> getMids(){
    return mids_l;
  }
  
  /*Functions to print to the console the list of nodes 
  and the list of cells as a matrix
  */
  void printGrid(){
    println("Grid:");
    for (int e:nodes){
        if(e>=0&&e<=9){
          print(" "+e+" ");
          if(e%gridSize==gridSize-1){
            println();
          }
        }else{
          print(e+" ");
          if(e%gridSize==gridSize-1){
            println();
         }
       }  
     }
   }
   
  //Print to the console the list of cells as a matrix
  void printCells(){
    println("Cells:");
    for (int e:cells){
        if(e>=0&&e<=9){
          print(" "+e+" ");
          if(e%(gridSize-1)==gridSize-2){
            println();
          }
        }else{
          print(e+" ");
          if(e%(gridSize-1)==gridSize-2){
            println();
         }
       }  
     }
   }
   
  //Function whether a node is in the grid 
  boolean hasNode(int o){
    if(nodes.hasValue(o)){
        return true;
      }else{
        println(o+" is not in the grid");
        return false;
    }
  }
   
  //Function to get coordinates of a node
  void getCoordinates(int n){
    if(hasNode(n)){
      int nod = n;
      int count=0;
      coord = new IntList();
      for (int e = 0; e<(nodes.size()/gridSize); e++){
        for (int f = 0; f<(nodes.size()/gridSize); f++){
          count++;
          if (nod == count-1){
            coord.append(f); 
            coord.append(e);
            print("Coordinates of "+n+": ");
            println(f+", "+e);
          }
        }
      }
    }
  }
  
  //Funtion to determine the type of node, 
  //i.e., corner, side, or middle 
  //and its number of possible connections
  void nConnections(int m){
    if(corners_l.contains(m)){
      connections = new int[2];
      pos = new String[2];
      println(m+" has "+connections.length+" connections");
    }else if(sides_l.contains(m)){
      connections = new int [3];
      pos = new String[3];
      println(m+" has "+connections.length+" connections");
    }else if(mids_l.contains(m)){
      connections = new int [4];
      pos = new String[4];
      println(m+" has "+connections.length+" connections");
    }
  }
  
  //Funtion to find the connections for every node
  void connectionsPerN(int n){
    nConnections(n);
    //Corners
    if (connections.length==2){
      int nPos = corners_l.indexOf(n);
      if (nPos == 0){
        
        connections[0] = n+1;
        pos[0] = "R";
        
        connections[1] = n+gridSize;
        pos[1] = "D";
        
      }else if(nPos == 1){
        connections[0] = n+gridSize;
        pos[0] = "D";
        
        connections[1] = n-1;
        pos[1] = "L";
        
      }else if(nPos == 2){
        connections[0] = n+1;
        pos[0] = "R";
        
        connections[1] = n-gridSize;
        pos[1] = "U";
        
      }else if(nPos==3){
        connections[0] = n-1;
        pos[0] = "L";
        
        connections[1] = n-gridSize;
        pos[1] = "U";
        
        }
    }
    //Sides
    if (connections.length==3){
       int oddPair = 1;//odd 
       if(gridSize%2==0){
          oddPair = 2;//pair
         }
      int dPos = sides_l.size()/4;//4 because it is a squared grid
      //println("dPos: "+dPos+" "+sides_l.size());
      int nPos = sides_l.indexOf(n)/dPos;
      //int nPos = sides_l.indexOf(n)%(gridSize-1);      
      //Pair grids 
      if(oddPair==2 && sides_l.indexOf(n)<dPos){
          nPos = 0;
        }else if (oddPair==2 && sides_l.indexOf(n)>=(dPos*3)){
          nPos = 3;
        }else if(oddPair==2 && sides_l.indexOf(n)%2==0){
          nPos = 1;
        }else if(oddPair==2 && sides_l.indexOf(n)%2==1){
          nPos = 2;
        }
      //Odd grids 
      if(oddPair==1 && sides_l.indexOf(n)<dPos){
          nPos = 0;
        }else if (oddPair==1 && sides_l.indexOf(n)>=(dPos*3)){
          nPos = 3;
        }else if(oddPair==1 && sides_l.indexOf(n)%2==1){
          nPos = 1;
        }else if(oddPair==1 && sides_l.indexOf(n)%2==0){
          nPos = 2;
        }
      
      if (nPos == 0){
        connections[0] = n+1;
        pos[0] = "R";
        connections[1] = n+gridSize;
        pos[1] = "D";
        connections[2] = n-1;;
        pos[2] = "L";
      }else if(nPos == 1){
        connections[0] = n+1;
        pos[0] = "R";
        connections[1] = n+gridSize;
        pos[1] = "D";
        connections[2] = n-gridSize;
        pos[2] = "U";
      }else if(nPos == 2){
        connections[0] = n+gridSize;
        pos[0] = "D";
        connections[1] = n-1;
        pos[1] = "L";
        connections[2] = n-gridSize;
        pos[2] = "U";
      }else if(nPos == 3){
        connections[0] = n+1;
        pos[0] = "R";
        connections[1] = n-1;
        pos[1] = "L";
        connections[2] = n-gridSize;
        pos[2] = "U";
      }
    }
    //Middle
    if (connections.length==4){
      connections[0] = n+1;
      pos[0] = "R";
      connections[1] = n+gridSize;
      pos[1] = "D";
      connections[2] = n-1;
      pos[2] = "L";
      connections[3] = n-gridSize;
      pos[3] = "U";
    }
    println(connections);
    println(pos);
  }
  
  boolean hasConnection(int m, int n){
    connectionsPerN(m);
    boolean hConn = false;
    for(int i:connections){
      if(i==n){
        hConn = true;
        }
      }
    return hConn;
  }
  
  /*Functions to draw on the canvas (i) the grid 
  and (ii) connections between the nodes 
  */
  //Draw the grid
  void drawGrid(){
    int xSpacer = width/gridSize;
    int ySpacer = height/gridSize;
    int x;
    int y;
    for (int e:nodes){
      getCoordinates(e);
      x = (coord.get(0)*xSpacer)+xSpacer/2;
      y = (coord.get(1)*ySpacer)+ySpacer/2;
      point(x, y);
      text("("+coord.get(0)+", ",x,y);
      text(coord.get(1)+")",x+textSize,y);
    }      
  }  
  //Draw a connection
  void drawConn (int o){
    int node = o;
    int xSpacer = width/gridSize;
    int ySpacer = height/gridSize;
    
    connectionsPerN(node);
    getCoordinates(node);
    int nodeOneX = (coord.get(0)*xSpacer)+xSpacer/2;
    int nodeOneY = (coord.get(1)*xSpacer)+ySpacer/2;
    
    for (int i=0; i<connections.length;i++){
      getCoordinates(connections[i]);
      int nodeTwoX = (coord.get(0)*xSpacer)+ySpacer/2;
      int nodeTwoY = (coord.get(1)*xSpacer)+ySpacer/2;
      line(nodeOneX,nodeOneY,nodeTwoX,nodeTwoY);
      
      }
  }
  
}
