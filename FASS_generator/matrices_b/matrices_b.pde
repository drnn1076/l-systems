

int gridSize = 3;
int tNodes = int(sq(gridSize));
int tCells = int(sq(gridSize-1));
int textSize = 12;
 
IntList nodes_l = new IntList();
IntList cells_l = new IntList();
ArrayList<Integer> corners_l = new ArrayList<Integer>();
ArrayList<Integer> sides_l = new ArrayList<Integer>();
ArrayList<Integer> mids_l = new ArrayList<Integer>();
IntList coord;

int[] connections; 
String[] pos;
IntList normals = new IntList(); 


void setup(){
  size(480,480);
  background(#FFFFFF);
  strokeWeight(6);
  fill(#000000);
  textSize(textSize);

  //populate the list of nodes in the grid
  for (int i = 0; i<tNodes; i++){
    nodes_l.append(i);
  }
  //Populate the list of cells in the grid
  for (int i = 0; i<tCells; i++){
    cells_l.append(i);
  }

  
  //Print the list of nodes and cells
  println("List of nodes: "+nodes_l);
  println("List of cells: "+ cells_l);
  
  //Create an object for the graph
  Graph g2 = new Graph(nodes_l, cells_l);
  //g2.printLists();
  //g2.printGrid();
  //g2.printCells();
  g2.drawGrid();

  for (int i=0; i<tNodes; i++){
    g2.connectionsPerN(i);
  }
  
  
  //Draw connections for a node
  strokeWeight(0.5);
  stroke(#FF0000);
  //Draw connections for the corner, sides, and/or middle nodes
  //g2.getCorners();
  //g2.getSides();
  //g2.getMids();
  
  //Draw all connections
  for (int i=0;i<nodes_l.size();i++){
    g2.drawConn(i);
  }
  
  //Draw connections for corner nodes
  //for (int i=0; i<corners_l.size(); i++){
  //  g2.drawConn(corners_l.get(i));
  //}
  
  //Draw connections for the side nodes
  //for (int i=0; i<sides_l.size(); i++){
  //  g2.drawConn(sides_l.get(i));
  //}
  
  //Draw connections for the middles nodes
  //for (int i=0; i<mids_l.size(); i++){
  //  g2.drawConn(mids_l.get(i));
  //}
  
 
  
  
  //if (g2.hasConnection(testN, connections[0])){
  //  println(true+", "+testN+" is connected to "+connections[0]);
  //}
  
  //int [] path = {0,1,2,5,8};
  //edge[] ed = new edge[connections.length];
  //for (int k=0; k<path.length;k++){
  //  g2.getCoordinates(k);
  //  g2.connectionsPerN(k);
  //  for (int i=0; i<ed.length;i++){
  //    ed[i] = new edge(coord.get(0), coord.get(1), normal, pos[i]);
  //    //ed[i].vEdge();
  //    ed[i].drawEdge();
  //  }
  //}
  
  //0=in, 1=out AND R=Right, D=Down, L=Left, U=Up
  int[] testN = {4};
  for (int i:testN){
    g2.getCoordinates(i);
    g2.connectionsPerN(i);
    edge[] ed = new edge[connections.length];
    for (int j=0; j<ed.length;j++){
      ed[j] = new edge(coord.get(0), coord.get(1), 1, pos[j]);
      ed[j].vEdge();
      ed[j].drawEdge();
  }
  }
  
  
   
  
  //frameRate(1); 
}

//int node_an = 0;
//Graph g3 = new Graph(nodes_l, cells_l);
//void draw(){
  
//  //Draw connections for a node
//  g3.drawConn(node_an);
//  node_an++;
//  if(node_an>=tNodes){
//    node_an = 0;
//  }
//}
