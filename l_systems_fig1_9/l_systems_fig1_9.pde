/* Programa para escribir cadenas para interpretación en
sistemas L con el sistema de tortuga, 
Referencia: Prusinkiewicz, Przemyslaw, y Aristid Lindenmayer. 1996. 
The algorithmic beauty of plants. Berlin, DE: Springer. P.7

Autores
Pablo Sanchez Mejía 
Valentina Vasco Restrepo  
Ricardo Cedeño Montaña 
Semillero Imagen Algoritmo 
Facultad de Comunicaciones y Filología 
Universidad de Antioquia 2024
*/

//Importar librería para exportar archivos svg
import processing.svg.*; 

//Definiciones
/*
Estas definiciones corresponden en el libro de referencia a: 
fig 1.9: A sequence of Koch curves obtained by successive modification of the production successor (p. 10)

Este código permite realizar la fig 1.9
Axiomas
(a) String w = "F-F-F-F";
(b) String w = "F-F-F-F";
(c) String w = "F-F-F-F";
(d) String w = "F-F-F-F";
(e) String w = "F-F-F-F";
(f) String w = "F-F-F-F";

regla
(a) String F = "FF-F-F-F-F-F+F";
(b) String F = "FF-F-F-F-FF";
(c) String w = "FF-F+F-F-FF";
(d) String w = "FF-F--F-F";
(e) String w = "F-FF--F-F";
(f) String w = "F-F+F-F-F";

iteraciones
(a) int n = 4;
(b) int n = 4;
(c) int n = 3;
(d) int n = 4;
(e) int n = 5;
(f) int n = 4;

*/
//Axioma
String w = "F-F-F-F";
//Variables para copiar el axioma y crear la cadena final
String wCopy = w;
String wFinal = wCopy;
//Reglas
String f = "f";
String F = "F-F+F-F-F";

//Iteraciones
int n = 4;
//Longitud de la línea
int lenF = 12;
//Ángulo
float d = radians(90.0);

void setup() {
  //Configuración básica
  size(1000, 1000);
  background(255);
  //Iniciar a exportar archivo SVG
  beginRecord(SVG, "fig_1.9f.svg");
  //Llamar la función para escribir cadenas.
  stringWriter(n, wCopy);
  println("Cadena: "+ wFinal);
  //Llamar la función para graficar.
  drawer();
  //Terminar de exportar archivo SVG.
  endRecord();
}  
