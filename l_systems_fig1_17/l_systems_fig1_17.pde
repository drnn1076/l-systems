/* Programa para escribir cadenas para interpretación en
sistemas L con el sistema de tortuga, 
Referencia: Prusinkiewicz, Przemyslaw, y Aristid Lindenmayer. 1996. 
The algorithmic beauty of plants. Berlin, DE: Springer. P.7

Autores
Pablo Sanchez Mejía 
Valentina Vasco Restrepo  
Ricardo Cedeño Montaña 
Semillero Imagen Algoritmo 
Facultad de Comunicaciones y Filología 
Universidad de Antioquia 2024
*/

//Importar librería para exportar archivos svg
import processing.svg.*; 

//Definiciones
/*
Estas definiciones corresponden en el libro de referencia a: 
fig 1.17: Peano curve (p. 17)

(a) Peano Curve:
//Axioma 
String w = "L";
//Reglas
String F = "F";
String L = "LFRFL-F-RFLFR+F+LFRFL";
String R = "RFLFR+F+LFRFL-F-RFLFR";
(b) 
//Axioma
String w = "-R";
//Reglas
String F = "F";
String L = "L+F+R-F-L+F+R-F-L-F-R+F+L-F-R-F-L+F+R-F-L-F-R-F-L+F+R+F+L+F+R-F-L+F+R+F+L-RFF+L+F+R-F-L+F+R-F-L";
String R = "R-F-L+F+R-F-L+F+R+F+L-F-R+F+L+F+R-F-L+F+R+F+L+F+R-F-L-F-R-F-L+F+R-F-L-F-R+F+L-F-R-F-L+F+R-F-L+F+R";
float d = radians(45.0);
*/
//Axioma
String w = "L";
//Variables para copiar el axioma y crear la cadena final
String wCopy = w;
String wTemp = "";
String wFinal = "";
//Reglas
String F = "F";
String f = "";
String L = "LFRFL-F-RFLFR+F+LFRFL";
String R = "RFLFR+F+LFRFL-F-RFLFR";


/*Notas:
En el drawer los tramos de R y L no deben tener linea ni
translación para que la curva sea proporcial.
*/

//Iteraciones
int n = 4;
//Longitud de la línea
int lenF = 6;
//Ángulo
float d = radians(90.0);

void setup() {
  //Configuración básica
  size(1000, 1000);
  background(255);
  //Iniciar a exportar archivo SVG
  //beginRecord(SVG, "fig_1.17a.svg");
  //Llamar la función para escribir cadenas.
  stringWriter(n, wCopy);
  println("Cadena: "+ wFinal);
  //Llamar la función para graficar.
  drawer();
  //Terminar de exportar archivo SVG.
  endRecord();
}  
