# L-systems

## Name
Sistemas L en processing  
## Description
Sistemas L escritos en processign para ploteo. Los sketches corresponden a los ejemplos del libro Prusinkiewicz, Przemyslaw, y Aristid Lindenmayer. The algorithmic beauty of plants. Berlin, DE: Springer, 1996.

## Visuals
Estas son algunas de la imágenes que hemos obtenido:  
![](l-systems_png_img/fig_1.6.png){width=350 height=350} ![](l-systems_png_img/fig_1.7a.png){width=350 height=350}   
![](l-systems_png_img/fig_1.7b.png){width=350 height=350} ![](l-systems_png_img/fig_1.8.png){width=350 height=350}   
![](l-systems_png_img/fig_1.9a.png){width=350 height=350} ![](l-systems_png_img/fig_1.9b.png){width=350 height=350}  
![](l-systems_png_img/fig_1.9c.png){width=350 height=350} ![](l-systems_png_img/fig_1.9d.png){width=350 height=350}   
![](l-systems_png_img/fig_1.9e.png){width=350 height=350} ![](l-systems_png_img/fig_1.9f.png){width=350 height=350}  
![](l-systems_png_img/fig_1.10a.png){width=350 height=350} ![](l-systems_png_img/fig_1.10b.png){width=350 height=350}  
## Installation
Sketches para ser usados con processing.

## Roadmap
Cada figura o ejemplo tendrá un sketch propio.

## Contributing
Cerradas a miembros del semillero Imagen Algortimo de la Universidad de Antioquia, Medellín, Colombia

## Authors and acknowledgment
- Pablo Sanchez Mejía 
- Valentina Vasco Restrepo  
- Ricardo Cedeño Montaña 

Semillero Imagen Algoritmo 
Facultad de Comunicaciones y Filología 
Universidad de Antioquia 2024
## License
 <>
    Copyright (C) 2024 Universidad de Antioquia, Ricardo Cedeño Montaña, Pablo Sanchez Mejía y Valentina Vasco Restrepo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Project status
2024 primer código. 