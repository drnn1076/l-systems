/* Programa para escribir cadenas para interpretación en
sistemas L con el sistema de tortuga, 
Referencia: Prusinkiewicz, Przemyslaw, y Aristid Lindenmayer. 1996. 
The algorithmic beauty of plants. Berlin, DE: Springer. P.7

Autores
Pablo Sanchez Mejía 
Valentina Vasco Restrepo  
Ricardo Cedeño Montaña 
Semillero Imagen Algoritmo 
Facultad de Comunicaciones y Filología 
Universidad de Antioquia 2024
*/

//Importar librería para exportar archivos svg
import processing.svg.*; 

//Definiciones
/*
Estas definiciones corresponden en el libro de referencia a: 
fig 1.8: Combination of islands and lakes (p. 9)

Este código permite realizar la fig 1.8
Axiomas
(a) String w = "F+F+F+F"

regla
(a) String F = "F+f-FF+F+FF+Ff+FF-f+FF-F-FF-Ff-FFF";
(a) String f = "ffffff";
*/
//Axioma
String w = "F+F+F+F";
//Variables para copiar el axioma y crear la cadena final
String wCopy = w;
String wTemp = "";
String wFinal = "";
//Reglas
String F = "F+f-FF+F+FF+Ff+FF-f+FF-F-FF-Ff-FFF";
String f = "ffffff";
//Iteraciones
int n = 2;
//Longitud de la línea
int lenF = 12;
//Ángulo
float d = radians(90.0);

void setup() {
  //Configuración básica
  size(1000, 1000);
  background(255);
  //Iniciar a exportar archivo SVG
  beginRecord(SVG, "fig_1.8.svg");
  //Llamar la función para escribir cadenas.
  stringWriter(n, wCopy);
  println("Cadena: "+ wFinal);
  //Llamar la función para graficar.
  drawer();
  //Terminar de exportar archivo SVG.
  endRecord();
}  
