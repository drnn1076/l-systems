/* Programa para escribir cadenas para interpretación en
sistemas L con el sistema de tortuga, 
Referencia: Prusinkiewicz, Przemyslaw, y Aristid Lindenmayer. 1996. 
The algorithmic beauty of plants. Berlin, DE: Springer. P.7

Autores
Pablo Sanchez Mejía 
Valentina Vasco Restrepo  
Ricardo Cedeño Montaña 
Semillero Imagen Algoritmo 
Facultad de Comunicaciones y Filología 
Universidad de Antioquia 2024
*/

//Importar librería para exportar archivos svg
import processing.svg.*; 

//Definiciones
/*
Estas definiciones corresponden en el libro de referencia a: 
fig 1.24: bracketed OL-Systems

(a) 
//Axioma 
String w = "F";
//Reglas
String F = "F[+F]F[-F]F";
float d = radians(25.7);
(b)
String F = "F[+F]F[-F]F";
float d = radians(20.0);
(c)
String F = "FF-[-F+F+F]+[+F-F-F]"
float d = radians(22.5)
(d)
String w = "L";
String L = "F[+X]F[-X]+X"
String F = "FF"
float d = radians(20)
(e)
String w = "L"
String L = "F[+L][-L]FL"
String F = FF
float d = radians(25.7)
(f)
String w = "L"
String L = "F-[[X]+X]+F[+FX]-X"
String F = "FF"
float d = radians(22.5)
*/
//Axioma
String w = "L";
//Variables para copiar el axioma y crear la cadena final
String wCopy = w;
String wTemp = "";
String wFinal = "";
//Reglas
String F = "FF";

String f = "";
String L = "F[+L][-L]FL";
String R = "RFLFR+F+LFRFL-F-RFLFR";


/*Notas:
En el drawer los tramos de R y L no deben tener linea ni
translación para que la curva sea proporcial.
*/

//Iteraciones
int n = 7;
//Longitud de la línea
int lenF = 3;
//Ángulo
float d = radians(30);

void setup() {
  //Configuración básica
  size(1000, 1000);
  background(255);
  //Iniciar a exportar archivo SVG
  //beginRecord(SVG, "fig_1.24_xxx.svg");
  //Llamar la función para escribir cadenas.
  stringWriter(n, wCopy);
  println("Cadena: "+ wFinal);
  //Llamar la función para graficar.
  drawer();
  //Terminar de exportar archivo SVG.
  endRecord();
}  
